import { User } from './../models/user';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  result: User[] = [];
  userForm: FormGroup;
  userInput: any;

  constructor(private fb: FormBuilder, private userService: ApiService ) { }

  ngOnInit(): void {
  }
  createUserForm(){
    return this.fb.group({
      id: [''],
      firstname: [''],
      lastname: [''],
    });
  }

  createUserPost(){
    const user = new User();
    user.firstname = this.userForm.controls.firstname.value;
    user.lastname = this.userForm.controls.lastname.value;
    this.userService
    .postUser(user)
    .toPromise()
    .then((res) => {
      this.result.push(res);
    });
  }
  submitInfo = () => {
    let user: User = {
      firstname: this.userInput,
      lastname: this.userInput
    };
    this.userService.postUser(user).toPromise().then((res) => {
    console.log(res)
    })
    .catch((err) => {
      console.log(err);
    });
  }

}
